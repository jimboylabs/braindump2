---
date: 2023-07-10
title: How to Install PHP 8.1 on Ubuntu
tags:
categories:
lastMod: 2023-07-10
---
```bash
sudo apt install php8.1-fpm php8.1-mysql \
	php8.1-mbstring php8.1-xml php8.1-bcmath \
    php8.1-sqlite3 php8.1-curl php8.1-zip
```
