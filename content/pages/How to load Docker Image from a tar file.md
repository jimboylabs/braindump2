---
date: 2023-07-10
title: How to load Docker Image from a tar file
tags:
categories:
lastMod: 2023-07-10
---
Notes

```bash
docker load /tmp/newproject.tar
```
