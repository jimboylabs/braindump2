---
date: 2023-06-29
title: WSL
tags:
categories:
lastMod: 2023-06-29
---
## Slipnotes

  + [windows subsystem for linux - How to add second WSL2 Ubuntu distro (fresh install) - Super User](https://superuser.com/questions/1515246/how-to-add-second-wsl2-ubuntu-distro-fresh-install)

  + Download Ubuntu Image

    + [Index of /wsl/jammy/current (ubuntu.com)](https://cloud-images.ubuntu.com/wsl/jammy/current/)

  + 21:13 just learn in [WSL]({{< ref "/pages/WSL" >}}) we can paste the clipboard output using this command


  + 17:24 find this guide related to managing SSH in [WSL]({{< ref "/pages/WSL" >}}) might be useful in the future [Sharing SSH keys between Windows and WSL 2 - Windows Command Line (microsoft.com)](https://devblogs.microsoft.com/commandline/sharing-ssh-keys-between-windows-and-wsl-2/)


  + 


