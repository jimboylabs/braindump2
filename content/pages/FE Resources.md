---
date: 2023-06-28
title: FE Resources
tags:
categories:
lastMod: 2023-06-29
---
## Slipnotes

  + frontendmentor.io (free & paid challenges)

  + frontendpractice.com (recreate real websites)

  + stylestage.dev (practice your CSS skills)

  + firsttimersonly.com (get started with open source)

  + [Figma 101: Introduction to Figma | Designlab](https://designlab.com/figma-101-course/introduction-to-figma/)

  + https://instagram.com/zanderwhitehurst

  + https://www.figma.com/community/file/883657911603554993/Essential-UI---Figma-Ui-Kit

  + 
